package main.coderindigo.com.countdown.activity.helper;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import main.coderindigo.com.countdown.activity.MainActivity;
import main.coderindigo.com.countdown.activity.act.AlarmNotify;
import main.coderindigo.com.countdown.activity.braodcast.AlarmReciever;

/**
 * Created by sujana on 12/25/2015.
 */
public class Utils {



    public static void cancelAlarm(int id, Context context) {

        //Log.d(TAG, id + "ssssssssssssssssssssssssssssssssssssssssssssssssssss");
        //myIntent = getIntent();
        //PendingIntent.getBroadcast(MainActivity.this, id, myIntent, 0).cancel();
        //pIntent.getCreatorUid();
        // alarmManager.cancel(pIntent);
        Intent intent = new Intent(context, AlarmReciever.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, id, intent,
                0);
        MainActivity.alarmManager.cancel(sender);
    }

    public static Intent createInCallIntent(Context c) {
        Intent intent = new Intent(c, AlarmNotify.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS
                | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
        intent.setClassName("<package>", AlarmNotify.class.getName());
        return intent;
    }

}
