package main.coderindigo.com.countdown.activity.braodcast;

/**
 * Created by sujana on 12/19/2015.
 */


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Toast;

import main.coderindigo.com.countdown.R;
import main.coderindigo.com.countdown.activity.MainActivity;
import main.coderindigo.com.countdown.activity.act.AlarmNotify;
import main.coderindigo.com.countdown.activity.helper.WakeLock;
import main.coderindigo.com.countdown.activity.service.AlarmService;

public class AlarmReciever extends BroadcastReceiver {
    private static final String TAG = AlarmReciever.class.getSimpleName();
    private NotificationManager alarmNotificationManager;

    public AlarmReciever() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();

        Bundle b = bundle.getBundle("extras");

        String time = b.getString("TIME");
        Integer id = b.getInt("remCounter");
        Log.d(TAG, "id" + id);
        Log.d(TAG, "Alarm reciever : " + System.currentTimeMillis() + "tome :" + time + " ====" + bundle.getInt(Intent.EXTRA_ALARM_COUNT));
//        Intent i = new Intent(context, AlarmNotify.class);
//        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        context.startActivity(i);
        Toast.makeText(context, "ALarm On", Toast.LENGTH_SHORT).show();
        WakeLock.acquire(context);

//        Intent service = new Intent(context, AlarmService.class);
//        service.putExtra("id", id + "");
//        //service.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        context.startService(service);
        Log.i(TAG, "Starting service @ " + SystemClock.elapsedRealtime());
        context.startService(new Intent(context, AlarmService.class));
        Log.i(TAG, "Started");


    }


//        ComponentName comp = new ComponentName(context.getPackageName(),
//                AlarmService.class.getName());
//        Log.d(TAG, "Alarm 1 : ________________" + context.getPackageName()+" " +
//                AlarmService.class.getName());
//        startWakefulService(context, (intent.setComponent(comp)));
//        Log.d(TAG, "Alarm 2 : " );
//        setResultCode(MainActivity.RESULT_OK);
//        Log.d(TAG, "Alarm 3 : " );


}
