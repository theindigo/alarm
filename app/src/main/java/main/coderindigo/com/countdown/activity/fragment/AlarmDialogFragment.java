package main.coderindigo.com.countdown.activity.fragment;


import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import main.coderindigo.com.countdown.R;
import main.coderindigo.com.countdown.activity.helper.Constants;

/**
 * A simple {@link Fragment} subclass.
 */
public class AlarmDialogFragment extends DialogFragment {


    public AlarmDialogFragment() {
        // Required empty public constructor
    }


    private int timeHour;
    private int timeMinute;
    private Handler handler;

    public AlarmDialogFragment(Handler handler) {
        this.handler = handler;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //Bundle bundle = getArguments();
//        timeHour = bundle.getInt(Constants.HOUR);
//        timeMinute = bundle.getInt(Constants.MINUTE);


        TimePickerDialog.OnTimeSetListener listener;
        listener = new TimePickerDialog.OnTimeSetListener() {
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                timeHour = hourOfDay;
                timeMinute = minute;


                String amPm;
                if (hourOfDay < 12) {
                    amPm = "AM";
                } else {
                    amPm = "PM";
                }
                Bundle b = new Bundle();
                b.putInt(Constants.HOUR, timeHour);
                b.putInt(Constants.MINUTE, timeMinute);
                b.putString(Constants.AM_PM, amPm);


                Message msg = new Message();
                msg.setData(b);
                handler.sendMessage(msg);
            }
        };
        return new TimePickerDialog(getActivity(), listener, timeHour, timeMinute, false);
    }


}
