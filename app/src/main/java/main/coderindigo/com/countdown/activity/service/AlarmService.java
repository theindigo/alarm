package main.coderindigo.com.countdown.activity.service;

/**
 * Created by sujana on 12/20/2015.
 */

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import main.coderindigo.com.countdown.R;
import main.coderindigo.com.countdown.activity.MainActivity;
import main.coderindigo.com.countdown.activity.act.AlarmNotify;
import main.coderindigo.com.countdown.activity.braodcast.AlarmReciever;
import main.coderindigo.com.countdown.activity.helper.Utils;
import main.coderindigo.com.countdown.activity.helper.WakeLock;

public class AlarmService extends Service {
    private static final String TAG = AlarmService.class.getSimpleName();
    private NotificationManager alarmNotificationManager;
    private static int reqCode = 0;

    MediaPlayer player;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Toast.makeText(this, "My Service Created", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onCreate");

        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
//        Ringtone ringtone = RingtoneManager.getRingtone(context, uri);
//        ringtone.s;

        player = MediaPlayer.create(this, uri);

        player.setLooping(false);

         Intent i =  new Intent(this, AlarmNotify.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
         // Set looping
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "My Service Stopped", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onDestroy");
        player.stop();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "My Service Started", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onStart");
        player.start();

        return super.onStartCommand(intent, flags, startId);
    }

//    @Override
//    public void onHandleIntent(Intent intent) {
//        Log.d(TAG, "Preparing to send notification...: ");
//        //sendNotification("Wake Up! Wake Up!");
////        Log.i(TAG, "Completed service @ " + SystemClock.elapsedRealtime());
//        int id = Integer.parseInt(intent.getStringExtra("id"));
//
//        Log.i(TAG, "id========== " + id);
////        PendingIntent contentIntent = PendingIntent.getActivity(this, id,
////                new Intent(context, AlarmNotify.class), 0);
////
//
//        Intent i = new Intent(this, AlarmNotify.class);
//        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////
//        // WakeLock.release();
//        PendingIntent inCallPendingIntent =
//                PendingIntent.getActivity(this, id,
//                        i, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        final Notification.Builder builder = new Notification.Builder(this);
//        builder.setSmallIcon(R.mipmap.ic_launcher);
////        if (image != null) {
////            builder.setLargeIcon(image);
////        }
//        builder.setOngoing(true);
//
//
//
//
//        //builder.setContentIntent(inCallPendingIntent);
//
//        builder.setContentText("clcik me");
//        builder.setUsesChronometer(false);
//
//        builder.setContentTitle("Alarm");
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            builder.setCategory(Notification.CATEGORY_ALARM);
//        }
//        //builder.setVisibility(Notification.VISIBILITY_PUBLIC);
//
//        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE), AudioManager.STREAM_RING);
//        builder.setVibrate(new long[]{500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500});
//
//        builder.setPriority(Notification.PRIORITY_MAX);
//        //builder.setTicker(title);
//        builder.setFullScreenIntent(inCallPendingIntent, true);
////
////        builder.addAction(R.drawable.alarm_image, "Reject", CallService.getPendingIntent(uber, callJson, CallService.REJECT));
////        builder.addAction(R.drawable.play_image, "Answer", CallService.getPendingIntent(uber, callJson, CallService.ANSWER));
//
//        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
//        Notification notification = builder.build();
//        notificationManager.notify(id, notification);
//
//
//        Log.i(TAG, "notification sent " );
//
//
//    }


}
