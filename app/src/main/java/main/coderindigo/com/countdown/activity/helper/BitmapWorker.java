package main.coderindigo.com.countdown.activity.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

/**
 * Created by sujana on 12/23/2015.
 */

public class BitmapWorker extends AsyncTask<Integer, Void, Bitmap> {
    //private final WeakReference<ImageView> imageViewReference;
    private int data;
    private Context context;
    private int x;
    private int y;

    private ImageView imageView;

    public BitmapWorker(ImageView imageView, Context con, int x , int y) {
        // Use a WeakReference to ensure the ImageView can be garbage collected
        //imageViewReference = new WeakReference<ImageView>(imageView);
        this.imageView = imageView;
        this.context = con;
        this.x = x;
        this.y = y;
    }

    // Decode image in background.
    @Override
    protected Bitmap doInBackground(Integer... params) {
        data = params[0];
        return BitmapHelper.decodeSampledBitmapFromResource(context.getResources(), data, x, y);
    }

    // Once complete, see if ImageView is still around and set bitmap.
    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (imageView != null && bitmap != null) {
           // imageView = imageView ;
            if (true) {

                imageView.setImageBitmap(bitmap);
            }
        }
    }
}