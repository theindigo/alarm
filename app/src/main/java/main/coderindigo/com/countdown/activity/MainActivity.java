package main.coderindigo.com.countdown.activity;


import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import main.coderindigo.com.countdown.activity.act.EditAlarm;
import main.coderindigo.com.countdown.activity.act.SettingsActivity;
import main.coderindigo.com.countdown.activity.adapter.RVAdapter;
import main.coderindigo.com.countdown.activity.animation.ViewPagerAnimation;
import main.coderindigo.com.countdown.activity.braodcast.AlarmReciever;
import main.coderindigo.com.countdown.activity.database.DbHandler;
import main.coderindigo.com.countdown.activity.fragment.*;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import main.coderindigo.com.countdown.R;
import main.coderindigo.com.countdown.activity.helper.Constants;
import main.coderindigo.com.countdown.activity.helper.CountdownTimer;
import main.coderindigo.com.countdown.activity.adapter.PageAdapter;
import main.coderindigo.com.countdown.activity.helper.Utils;
import main.coderindigo.com.countdown.activity.model.Alarm;

public class MainActivity extends AppCompatActivity implements CountDown.OnTimerStartListener, Dialog.OnClickListener, AlarmFragment.OnAddAlarmListner, RVAdapter.onCheck {

    //countdown
    private static final String TAG = MainActivity.class.getSimpleName();
    public static AlarmManager alarmManager;
    public static boolean onCreate = false;
    public RecyclerView rv;
    List<Alarm> alarms;
    RVAdapter recyclerViewAdapter;
    DbHandler db;
    Intent myIntent;
    private int timeHour;
    private int timeMinute;
    private PendingIntent pendingIntent;
    //private int reqCode = 0;
    private SeekBar seekBar_min;
    private SeekBar seekBar_sec;
    private CountDown mDisplayFragment;
    private AlarmFragment mAlarmFragment;
    private Handler handler;
    private TextView userInputMin;
    private TextView userInputSec;
    private CountdownTimer timer;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.alarm_image,
            R.drawable.timer_image,
    };


    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        //getSupportActionBar().setElevation(0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        setContentView(R.layout.activity_main);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        db = UpWithSmile.dbHandler;

        InitializeTabs();
        handler = new Handler();

//      countdown timer
        timer = new CountdownTimer(handler) {
            @Override
            public void onPlayNotification() {
                playSound();
            }

            @Override
            public void onTimerFinished() {

            }

            @Override
            public void onTimerStopped() {
                mDisplayFragment.updateTime("00:00");
            }

            @Override
            public void updateUI(long time) {
//                mDisplayFragment = (CountDown)getSupportFragmentManager().findFragmentByTag(
//                        "android:switcher:"+ R.id.pager+":0");
                if (mDisplayFragment != null) {
                    Log.d(TAG, "Im inside update");
                    // mDisplayFragment.updateTime(CountdownTimer.covertToString(time));
                    update(CountdownTimer.covertToString(time));

                } else {
                    Toast.makeText(MainActivity.this, "oopppss", Toast.LENGTH_SHORT).show();
                    // Log.d(TAG, mDisplayFragment.toString());
                }
            }
        };
    }

    public void update(String time) {
        mDisplayFragment.updateTime(time);
    }

    //play sound for countdown
    private void playSound() {
        try {
            AssetFileDescriptor afd = getAssets().openFd("sounds/alert.mp3");
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(afd.getFileDescriptor());
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //ondialog click countdown
    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                String minutes = userInputMin.getText().toString().trim();
                Log.d(TAG, "-----------" + minutes);

                String seconds = userInputSec.getText().toString().trim();
                String input = minutes + ":" + seconds;
                Log.d(TAG, "--TIME---" + input);
                if (CountdownTimer.isValid(input)) {
                    timer.setTimeRemaining(CountdownTimer.covertTomili(input));
                    timer.start();
                }
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                break;
        }
    }

    // initialize the tabs
    public void InitializeTabs() {
        viewPager = (ViewPager) findViewById(R.id.pager);
        final PageAdapter adapter = new PageAdapter(getSupportFragmentManager()) {
        };
        adapter.addFragment(new CountDown(), "Countdown");
        adapter.addFragment(new AlarmFragment(), "Alarm");
        viewPager.setAdapter(adapter);
        viewPager.setPageTransformer(true, new ViewPagerAnimation());
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        mDisplayFragment = (CountDown) adapter.getFragment(0);
        mAlarmFragment = (AlarmFragment) adapter.getFragment(1);
        setupTabIcons();
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
    }

    @Override
    public void OnTimerStartListener(View v) {
        if (v.getId() == R.id.start_button) {
            Toast.makeText(MainActivity.this, "ON", Toast.LENGTH_SHORT).show();
            LayoutInflater inflater = LayoutInflater.from(this);
            View view = inflater.inflate(R.layout.user_input, null);

            seekBar_min = (SeekBar) view.findViewById(R.id.seekBar_min);
            seekBar_sec = (SeekBar) view.findViewById(R.id.seekBar_sec);

            userInputMin = (TextView) view.findViewById(R.id.user_input_min);
            userInputSec = (TextView) view.findViewById(R.id.user_input_sec);
//            userInput.setText(seekBar.getProgress() );
            seekBar_min.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                int progress = 0;

                @Override
                public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                    progress = progresValue;
                    userInputMin.setText((progress < 10) ? "0" + progress : progress + "");
                    //Toast.makeText(getApplicationContext(), "Changing seekbar's progress", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override

                public void onStopTrackingTouch(SeekBar seekBar) {
                }

            });
            seekBar_sec.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                int progress = 0;

                @Override
                public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                    progress = progresValue;
                    userInputSec.setText((progress < 10) ? "0" + progress : progress + "");
                    //Toast.makeText(getApplicationContext(), "Changing seekbar's progress", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            });
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(view);
            builder.setCancelable(false);
            builder.setPositiveButton("OK", this);
            builder.setNegativeButton("CANCEL", this);
            builder.show();
        } else if (v.getId() == R.id.stop_button) {
            Toast.makeText(MainActivity.this, "OFF", Toast.LENGTH_SHORT).show();
            timer.stop();
        }
    }

    @Override
    public void onFragmentCreated(RVAdapter rvAdapter, RecyclerView r_v, List<Alarm> alarm) {
        recyclerViewAdapter = rvAdapter;
        // MainActivity.onCreate = true;
        this.alarms = alarm;
        this.rv = r_v;
        List<Alarm> al = db.getAllContacts();

        for (int i = 0; i < al.size(); i++) {
            alarms.add(al.get(i));
        }
        for (int i = 0; i < al.size(); i++) {
            Log.d(TAG, " >>>>>>>>>>>>" + alarms.get(i).getAlarmTime());
            //alarms = new ArrayList<>(alarms);
        }
        if (!al.isEmpty()) {
            Log.d(TAG, " /???????????????");
            rv.setAdapter(recyclerViewAdapter);
            recyclerViewAdapter.notifyDataSetChanged();
        }
    }

    //alram add callback
    @Override
    public void OnAddAlarmListner(View view, final List<Alarm> alarms) {
        Intent addIntent = new Intent(this, EditAlarm.class);
        addIntent.putExtra("REQUEST_CODE", 1);
        startActivityForResult(addIntent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        int reqCode = (int) System.currentTimeMillis();
        // check if the request code is same as what is passed  here it is 2


        if (resultCode == 1) {
            Bundle b = data.getBundleExtra("MESSAGE");
            timeHour = b.getInt(Constants.HOUR);
            timeMinute = b.getInt(Constants.MINUTE);
            Log.d(TAG, "on activityresult -----" + timeMinute + "=+++++" + timeHour);
            Calendar calendar = Calendar.getInstance();
            Calendar calSet = (Calendar) calendar.clone();
            calSet.set(Calendar.HOUR_OF_DAY, timeHour);
            calSet.set(Calendar.MINUTE, timeMinute);
            calSet.set(Calendar.SECOND, 0);
            calSet.set(Calendar.MILLISECOND, 0);

            if (calSet.compareTo(calendar) <= 0) {
                //Today Set time passed, count to tomorrow
                calSet.add(Calendar.DATE, 1);
            }
            Alarm alarm = new Alarm(reqCode, (timeHour >= 10 ? timeHour : "0" + timeHour) + "" + ":" + (timeMinute >= 10 ? timeMinute : "0" + timeMinute) + "", 1);
            myIntent = new Intent(MainActivity.this, AlarmReciever.class);
            Bundle extras = new Bundle();

            extras.putString("TIME", timeHour + "" + ":" + timeMinute + "");
            extras.putInt("remCounter", reqCode);

            myIntent.putExtra("extras", extras);

            Log.d(TAG, "ALARM __________ :" + calSet.getTimeInMillis());
            pendingIntent = PendingIntent.getBroadcast(MainActivity.this, alarm.getId(), myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calSet.getTimeInMillis(), pendingIntent);
            Log.d(TAG, "Inserting ..");
            Log.d(TAG, "Inserting .." + reqCode);

            db.addContact(alarm);
            Log.d(TAG, "handleMessage: " + alarms.size());
            Log.d(TAG, "recylcount: " + recyclerViewAdapter.getItemCount());
            alarms.add(recyclerViewAdapter.getItemCount(), alarm);
            recyclerViewAdapter.notifyItemInserted(recyclerViewAdapter.getItemCount());
            rv.scrollToPosition(recyclerViewAdapter.getItemCount() + 1);
        }
        else if (resultCode == 2){
            //edit and start intent
            Bundle b = data.getBundleExtra("MESSAGE");
            timeHour = b.getInt(Constants.HOUR);
            timeMinute = b.getInt(Constants.MINUTE);
            Log.d(TAG, "on edit Alarm -----" + timeMinute + "=+++++" + timeHour);
        }
    }

    @Override
    public void onSettingListner() {
        Intent intentSetPref = new Intent(getApplicationContext(), SettingsActivity.class);
        startActivity(intentSetPref);
    }



    @Override
    public void swipeRight(int position) {
        int id = alarms.get(position).getId();
        alarms.remove(position);
        UpWithSmile.dbHandler.deleteContact(id);
        recyclerViewAdapter.notifyDataSetChanged();
        Utils.cancelAlarm(id, this);
    }

    @Override
    public void onSwitchOn(int position) {
        Log.d(TAG, "In side activity chceck changed ON+++++++++ ");
        // int id = alarms.get(position).getId();
        startAlarm(position);
        Log.d(TAG, "Alarm started ");
        alarms.get(position).setStatus(1);
        Toast.makeText(getApplicationContext(), " alarm starerd", Toast.LENGTH_SHORT).show();
        db.updateContact(alarms.get(position));
        //recyclerViewAdapter.notifyItemChanged(position);
    }

    @Override
    public void onEditAlarm(int pos) {
        Log.d(TAG, pos+"  :  position pressed" );
        String[] splitTime =  alarms.get(pos).getAlarmTime().split(":");
        int hours = Integer.parseInt(splitTime[0]);
        int min = Integer.parseInt(splitTime[1]);
        Bundle bundle = new Bundle();
        bundle.putInt("hours", hours);
        bundle.putInt("min", min);
        Intent editIntent = new Intent(this, EditAlarm.class);
        editIntent.putExtra("EDIT_MESSAGE", bundle);
        editIntent.putExtra("REQUEST_CODE", 2);
        startActivityForResult(editIntent, 2);
    }

    @Override
    public void onSwitchOff(int pos) {
        Log.d(TAG, "In side activity chceck changed off ->>>>>>>>>>");
        int id = alarms.get(pos).getId();
        if (id != 0) {
            Utils.cancelAlarm(id, this);
            alarms.get(pos).setStatus(0);
            Log.d(TAG, "Alarm Stopped");
        }
        db.updateContact(alarms.get(pos));
        // recyclerViewAdapter.notifyItemChanged(pos);
    }


//    public void cancelAlarm(int id) {
//
//        Log.d(TAG, id + "ssssssssssssssssssssssssssssssssssssssssssssssssssss");
//        //myIntent = getIntent();
//        //PendingIntent.getBroadcast(MainActivity.this, id, myIntent, 0).cancel();
//        //pIntent.getCreatorUid();
//        // alarmManager.cancel(pIntent);
//        Intent intent = new Intent(MainActivity.this, AlarmReciever.class);
//        PendingIntent sender = PendingIntent.getBroadcast(MainActivity.this, id, intent,
//                0);
//        alarmManager.cancel(sender);
//    }

    private void startAlarm(int pos) {
        if (alarmManager != null) {
            //alarms.get()
            String string = alarms.get(pos).getAlarmTime();
            String[] parts = string.split(":");
            int timeHour = Integer.parseInt(parts[0]); // 004
            int timeMinute = Integer.parseInt(parts[1]); // 034556
            Log.d(TAG, " Lets turn the alarm o n " + timeHour + " " + timeMinute);

            Calendar calendar = Calendar.getInstance();
            Calendar calSet = (Calendar) calendar.clone();
            calSet.set(Calendar.HOUR_OF_DAY, timeHour);
            calSet.set(Calendar.MINUTE, timeMinute);
            calSet.set(Calendar.SECOND, 0);
            calSet.set(Calendar.MILLISECOND, 0);

            if (calSet.compareTo(calendar) <= 0) {
                //Today Set time passed, count to tomorrow
                calSet.add(Calendar.DATE, 1);
            }

            Intent myIntent = new Intent(MainActivity.this, AlarmReciever.class);
            Bundle extras = new Bundle();
            extras.putString("TIME", timeHour + "" + ":" + timeMinute + "");
            extras.putInt("remCounter", alarms.get(pos).getId());
            myIntent.putExtra("extras", extras);

            Log.d(TAG, "ALARM Started again :" + calSet.getTimeInMillis());
            pendingIntent = PendingIntent.getBroadcast(MainActivity.this, alarms.get(pos).getId(), myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calSet.getTimeInMillis(), pendingIntent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //insert to db in bactch
    }
}
