package main.coderindigo.com.countdown.activity.act;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.TimePicker;
import android.widget.Toast;

import main.coderindigo.com.countdown.R;
import main.coderindigo.com.countdown.activity.helper.Constants;

public class EditAlarm extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = EditAlarm.class.getSimpleName() ;
    boolean isChecked;
    CheckedTextView mon;
    CheckedTextView tue;
    CheckedTextView wed;
    CheckedTextView thu;
    CheckedTextView fri;
    CheckedTextView sat;
    CheckedTextView sun;

    TimePickerDialog.OnTimeSetListener listener;
    private int timeHour;
    private int timeMinute;
    Button done;
    Button cancel;
    String amPm ;
    TimePicker timePicker;
    Intent intent;
    int requestCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_alarm);
        mon = (CheckedTextView)findViewById(R.id.mon);
        tue = (CheckedTextView)findViewById(R.id.tue);
        wed = (CheckedTextView)findViewById(R.id.wed);
        thu = (CheckedTextView)findViewById(R.id.thu);
        fri = (CheckedTextView)findViewById(R.id.fri);
        sat = (CheckedTextView)findViewById(R.id.sat);
        sun = (CheckedTextView)findViewById(R.id.sun);
        mon.setOnClickListener(this);
        tue.setOnClickListener(this);
        wed.setOnClickListener(this);
        thu.setOnClickListener(this);
        fri.setOnClickListener(this);
        sat.setOnClickListener(this);
        sun.setOnClickListener(this);

        timePicker = (TimePicker) findViewById(R.id.timepicker);
        done = (Button)findViewById(R.id.done);
        cancel = (Button)findViewById(R.id.cancel);
        done.setOnClickListener(this);
        cancel.setOnClickListener(this);

        intent = getIntent();
        requestCode = getIntent().getExtras().getInt("REQUEST_CODE");
        Toast.makeText(this, "Code: " + requestCode, Toast.LENGTH_SHORT);
        if(requestCode == 2){
            Bundle bundle = getIntent().getExtras().getBundle("EDIT_MESSAGE");
            int hours = bundle.getInt("hours");
            int min = bundle.getInt("min");
            timePicker.setCurrentHour(hours);
            timePicker.setCurrentMinute(min);
        }
    }

    @Override
    public void onClick(View arg0) {
        Log.d(TAG, arg0.getId() +"");

        switch (arg0.getId()) {
            case R.id.mon:
                isChecked = mon.isChecked();
                Log.d(TAG, isChecked +"");
                if (isChecked)
                    mon.setChecked(false);
                else
                    mon.setChecked(true);
                break;
            case R.id.tue:
                isChecked = tue.isChecked();
                Log.d(TAG, isChecked +"");
                if (isChecked)
                    tue.setChecked(false);
                else
                    tue.setChecked(true);
                break;
            case R.id.wed:
                isChecked = wed.isChecked();
                Log.d(TAG, isChecked +"");
                if (isChecked)
                    wed.setChecked(false);
                else
                    wed.setChecked(true);
                break;
            case R.id.thu:
                isChecked = thu.isChecked();
                Log.d(TAG, isChecked +"");
                if (isChecked)
                    thu.setChecked(false);
                else
                    thu.setChecked(true);
                break;
            case R.id.fri:
                isChecked = fri.isChecked();
                Log.d(TAG, isChecked +"");
                if (isChecked)
                    fri.setChecked(false);
                else
                    fri.setChecked(true);
                break;
            case R.id.sat:
                isChecked = sat.isChecked();
                Log.d(TAG, isChecked +"");
                if (isChecked)
                    sat.setChecked(false);
                else
                    sat.setChecked(true);
                break;
            case R.id.sun:
                isChecked = sun.isChecked();
                Log.d(TAG, isChecked +"");
                if (isChecked)
                    sun.setChecked(false);
                else
                    sun.setChecked(true);
                break;

            case R.id.done:
                if(requestCode == 1) {
                    Intent addIntent = getTime();
                    setResult(1, addIntent);
                    finish();
                } else if(requestCode == 2){
                    Intent editIntent = getTime();
                    setResult(2, editIntent);
                    finish();
                }
            case R.id.cancel:
                finish();
        }
    }

    public Intent getTime(){
        Intent timeIntent = new Intent();
        int hour = timePicker.getCurrentHour();
        int min = timePicker.getCurrentMinute();
        Bundle b = new Bundle();
        Log.d(TAG, "on done -----" + hour + "=+++++" + min);

        b.putInt(Constants.HOUR, hour);
        b.putInt(Constants.MINUTE, min);
        b.putString(Constants.AM_PM, amPm);

        Log.d(TAG, "test done -----" + hour + "=+++++" + min);

        timeIntent.putExtra("MESSAGE", b);
        return timeIntent;
    }
}
