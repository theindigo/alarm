package main.coderindigo.com.countdown.activity.helper;

/**
 * Created by sujana on 12/19/2015.
 */

public abstract class Constants {
    public static final String HOUR = "time_hour";
    public static final String MINUTE = "time_minute";
    public static final String TIME_PICKER = "time_picker";
    public static final String AM_PM = "am_pm";
}