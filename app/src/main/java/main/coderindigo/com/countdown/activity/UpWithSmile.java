package main.coderindigo.com.countdown.activity;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.widget.ImageView;

import main.coderindigo.com.countdown.R;
import main.coderindigo.com.countdown.activity.database.DbHandler;

/**
 * Created by sujana on 12/23/2015.
 */
public class UpWithSmile extends Application {
    private static final String TAG = UpWithSmile.class.getSimpleName() ;

//	private static final String TAG = "RemindMe";

    public static DbHandler dbHandler;
    public static SQLiteDatabase db;
    //public static SharedPreferences sp;

//    public static final String TIME_OPTION = "time_option";
//    public static final String DATE_RANGE = "date_range";
//    public static final String DATE_FORMAT = "date_format";
//    public static final String TIME_FORMAT = "time_format";
//    public static final String VIBRATE_PREF = "vibrate_pref";
//    public static final String RINGTONE_PREF = "ringtone_pref";
//    public static final String DEFAULT_DATE_FORMAT = "yyyy-M-d";

    @Override
    public void onCreate() {
        super.onCreate();
//
//        PreferenceManager.setDefaultValues(this, R.xml.settings, false);
//        sp = PreferenceManager.getDefaultSharedPreferences(this);

        dbHandler = new DbHandler(this);
        db = dbHandler.getWritableDatabase();
        Log.d(TAG, "called when app creted");


    }

//    public static boolean showRemainingTime() {
//        return "1".equals(sp.getString(TIME_OPTION, "0"));
//    }
//
//    public static int getDateRange() {
//        return Integer.parseInt(sp.getString(DATE_RANGE, "0"));
//    }
//
//    public static String getDateFormat() {
//        return sp.getString(DATE_FORMAT, DEFAULT_DATE_FORMAT);
//    }
//
//    public static boolean is24Hours() {
//        return sp.getBoolean(TIME_FORMAT, true);
//    }
//
//    public static boolean isVibrate() {
//        return sp.getBoolean(VIBRATE_PREF, true);
//    }
//
//    public static String getRingtone() {
//        return sp.getString(RINGTONE_PREF, android.provider.Settings.System.DEFAULT_NOTIFICATION_URI.toString());
//    }

}

