package main.coderindigo.com.countdown.activity.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.github.brnunes.swipeablerecyclerview.SwipeableRecyclerViewTouchListener;

import java.util.ArrayList;
import java.util.List;

import main.coderindigo.com.countdown.R;
import main.coderindigo.com.countdown.activity.UpWithSmile;
import main.coderindigo.com.countdown.activity.adapter.RVAdapter;
//import main.coderindigo.com.countdown.activity.helper.RecyclerViewTouchListener;
import main.coderindigo.com.countdown.activity.helper.RecyclerViewTouchListener;
import main.coderindigo.com.countdown.activity.model.Alarm;


public class AlarmFragment extends Fragment implements View.OnClickListener {


    private static final String TAG = AlarmFragment.class.getSimpleName();
    List<Alarm> alarms;
    public RecyclerView rv;
    RVAdapter recyclerViewAdapter;
    FloatingActionButton addAlarm;
    FloatingActionButton setting;
    private OnAddAlarmListner mCallback;
    View view;

    public AlarmFragment() {
        // Required empty public constructor

        alarms = new ArrayList<>();
        //alarms.add(new Alarm(Integer.parseInt(cursor.getString(0)), "8:30", cursor.getInt(2)));

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_alarm, container, false);
        rv = (RecyclerView) view.findViewById(R.id.rv);
        addAlarm = (FloatingActionButton) view.findViewById(R.id.add_alarm);
        setting = (FloatingActionButton) view.findViewById(R.id.setting);
        addAlarm.setOnClickListener(this);
        setting.setOnClickListener(this);
        //rv.setOnClickListener(this);
        recyclerViewAdapter = new RVAdapter(alarms, getContext());
        rv.setHasFixedSize(true);
        rv.setAdapter(recyclerViewAdapter);

        SwipeableRecyclerViewTouchListener swipeTouchListener =
                new SwipeableRecyclerViewTouchListener(rv,
                        new SwipeableRecyclerViewTouchListener.SwipeListener() {
                            @Override
                            public boolean canSwipe(int position) {
                                return true;
                            }

                            @Override
                            public void onDismissedBySwipeLeft(RecyclerView recyclerView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                    int id = alarms.get(position).getId();
                                    alarms.remove(position);
                                    UpWithSmile.dbHandler.deleteContact(id);
                                    recyclerViewAdapter.notifyItemRemoved(position);
                                }
                                recyclerViewAdapter.notifyDataSetChanged();

                            }

                            @Override
                            public void onDismissedBySwipeRight(RecyclerView recyclerView, int[] reverseSortedPositions) {
//                                for (int position : reverseSortedPositions) {
//                                    mItems.remove(position);
//                                    mAdapter.notifyItemRemoved(position);
//                                }
//                                mAdapter.notifyDataSetChanged();
                                for (int position : reverseSortedPositions) {
                                   // int id = alarms.get(position).getId();
                                    mCallback.swipeRight(position);
                                   // recyclerViewAdapter.notifyItemRemoved(position);
                                }

                            }
                        });

        rv.addOnItemTouchListener(swipeTouchListener);

//        rv.addOnItemTouchListener(new RecyclerViewTouchListener(getContext(), rv, new RecyclerViewTouchListener.ClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                if (view.getId() == R.id.card_touch) {
//                   // Toast.makeText(getContext(), position + "clcik", Toast.LENGTH_SHORT).show();
//                    mCallback.onCardClick(position);
//
//                }
//
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//                Toast.makeText(getContext(), position + "LONG.", Toast.LENGTH_SHORT).show();
//
//            }
//
//            @Override
//            public void onSwipeRight(RecyclerView recyclerView, int position) {
//
//                Log.d(TAG, "swipeeee" + position + "");
//                int id = alarms.get(position).getId();
//                Log.d(TAG, "id" + id + "");
//                alarms.remove(position);
//                UpWithSmile.dbHandler.deleteContact(id);
//                recyclerViewAdapter.notifyItemRemoved(position);
//                recyclerViewAdapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onSwipeLeft(RecyclerView recyclerView, int position) {
//
//            }
//        }
//        ));
        return view;


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        rv.setLayoutManager(llm);


        mCallback.onFragmentCreated(recyclerViewAdapter, rv, alarms);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnAddAlarmListner) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnAddAlarmListner");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    public interface OnAddAlarmListner {
        void onFragmentCreated(RVAdapter rvAdapter, RecyclerView rv, List<Alarm> alarms);

        public void OnAddAlarmListner(View position, List<Alarm> alarms);
        void onSettingListner();
        void swipeRight(int position);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.setting) {
            mCallback.onSettingListner();

        } else if (v.getId() == R.id.add_alarm) {
            mCallback.OnAddAlarmListner(v, alarms);
        }


    }
}
