package main.coderindigo.com.countdown.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import main.coderindigo.com.countdown.R;
import main.coderindigo.com.countdown.activity.act.EditAlarm;
import main.coderindigo.com.countdown.activity.model.Alarm;


public class RVAdapter extends RecyclerView.Adapter<RVAdapter.AlarmViewHolder> {

    private static final String TAG = RVAdapter.class.getSimpleName();
    List<Alarm> alarms;
    Context context;
    private onCheck mCallback;

    public RVAdapter(List<Alarm> alarms, Context con) {
        this.alarms = alarms;
        this.context = con;
        try {
            mCallback = (onCheck) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnCheckListner");
        }
    }

    public RVAdapter() {
        super();

    }

    @Override
    public RVAdapter.AlarmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.alram_card, parent, false);
        AlarmViewHolder pv = new AlarmViewHolder(view, parent.getContext(), alarms, new AlarmViewHolder.OnCheckChanged() {
            @Override
            public void onSwitchOn(int position) {
                Toast.makeText(context, " inside view holderrrrrrrr", Toast.LENGTH_SHORT).show();
                if (alarms != null) {

                    mCallback.onSwitchOn(position);


                }

            }

            @Override
            public void onSwitchOff(int position) {

                mCallback.onSwitchOff(position);

            }

            @Override
            public void onEditAlarm(int position) {
                mCallback.onEditAlarm(position);
            }
        });

        return pv;
    }


    @Override
    public void onBindViewHolder(final RVAdapter.AlarmViewHolder holder, int position) {

        if (alarms != null) {
            holder.time.setText(alarms.get(position).getAlarmTime());
            Log.d(TAG, alarms.get(position).getStatus() + "");
            holder.alarmSwitch.setChecked(alarms.get(position).getStatus() == 1);
            holder.time.setVisibility(View.VISIBLE);
            //holder.onBind = false;
        } else {
            holder.time.setVisibility(View.GONE);
        }

        //   AlarmViewHolder.time.setText(alarms.get(position).getAlarmTime());

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return alarms.size();
    }

    @Override
    public void onViewAttachedToWindow(AlarmViewHolder holder) {
        super.onViewAttachedToWindow(holder);

    }

    public interface onCheck {
        public void onSwitchOn(int pos);
        public void onEditAlarm(int pos);
        public void onSwitchOff(int pos);
    }

    public static class AlarmViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final String TAG = AlarmViewHolder.class.getSimpleName();
        CardView cv;
        TextView time;
        LinearLayout linear;
        SwitchCompat alarmSwitch;
        Context con;
        List<Alarm> alarms;
        OnCheckChanged checkChange;
        private boolean onBind;

        public AlarmViewHolder(View itemView, final Context c, List<Alarm> al, final OnCheckChanged onCheckChanged) {
            super(itemView);
            this.con = c;
            this.alarms = al;
            this.checkChange = onCheckChanged;

            linear = (LinearLayout) itemView.findViewById(R.id.card_touch);
            cv = (CardView) itemView.findViewById(R.id.card_view);
            time = (TextView) itemView.findViewById(R.id.alarm_time);
            alarmSwitch = (SwitchCompat) itemView.findViewById(R.id.mySwitch);

            alarmSwitch.setOnCheckedChangeListener(new SwitchCompat.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    // do something, the isChecked will be
                    if (isChecked) {
                        // The toggle is enabled
                        alarms.get(getLayoutPosition()).setStatus(1);
                        if (checkChange != null) {
                            checkChange.onSwitchOn(getLayoutPosition());
                        }

                    } else {
                        // The toggle is disabled
                        if (checkChange != null) {
                            Log.d(TAG, getAdapterPosition() + " ++++++++++ " + alarms.get(getLayoutPosition()).getId());
                            checkChange.onSwitchOff(getLayoutPosition());
                        }
                        alarms.get(getLayoutPosition()).setStatus(0);
                    }


//                    int position = getLayoutPosition();
                    //switchChange(buttonView.isChecked(), position);

                    Log.d(TAG, "Swictch clicked" + getLayoutPosition() + " " + isChecked);
                    // true if the switch is in the On position


                    // notifyItemChanged(position);
                }
            });

            linear.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d(TAG, v.getId() + " Crd : " + R.id.card_touch + " switch : " + R.id.mySwitch);
            if (v.getId() == R.id.card_touch) {
                Log.d(TAG, "screen " + getLayoutPosition() + " ");
//                Intent i = new Intent(con, EditAlarm.class);
//                con.startActivity(i);
                checkChange.onEditAlarm(getLayoutPosition());
                Toast.makeText(con, "Screen clciked ", Toast.LENGTH_SHORT).show();
            }
        }

        public static interface OnCheckChanged {
            public void onSwitchOn(int position);
            public void onSwitchOff(int position);
            void onEditAlarm(int position);
        }
    }
}
