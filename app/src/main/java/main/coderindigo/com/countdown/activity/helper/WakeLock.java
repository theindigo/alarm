package main.coderindigo.com.countdown.activity.helper;

/**
 * Created by sujana on 12/25/2015.
 */

import android.os.PowerManager;


        import android.content.Context;
        import android.os.PowerManager;

import main.coderindigo.com.countdown.activity.MainActivity;

public abstract class WakeLock {
    private static PowerManager.WakeLock wakeLock;

    public static void acquire(Context ctx) {
        if (wakeLock != null) wakeLock.release();

        PowerManager pm = (PowerManager) ctx.getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |
                PowerManager.ACQUIRE_CAUSES_WAKEUP |
                PowerManager.ON_AFTER_RELEASE, "wakelock");
        wakeLock.acquire();
    }

    public static void release() {
        if (wakeLock != null) wakeLock.release(); wakeLock = null;
    }
}