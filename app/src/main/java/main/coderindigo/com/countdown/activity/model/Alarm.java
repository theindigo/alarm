package main.coderindigo.com.countdown.activity.model;

/**
 * Created by sujana on 12/19/2015.
 */
public class Alarm {

    private String alarmTime;
    private int status;
    private int id;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Alarm(){

    }

    public Alarm(int i, String alarmTime, int anInt){
        this.alarmTime = alarmTime;
        this.status = anInt;
        this.id = i;
    }

    public Alarm( String alarmTime, int anInt){
        this.alarmTime = alarmTime;
        this.status = anInt;

    }

    public String getAlarmTime() {
        return alarmTime;
    }

    public void setAlarmTime(String alarmTime) {
        this.alarmTime = alarmTime;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
