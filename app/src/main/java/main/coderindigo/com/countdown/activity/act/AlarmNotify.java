package main.coderindigo.com.countdown.activity.act;

import android.content.Context;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import main.coderindigo.com.countdown.R;
import main.coderindigo.com.countdown.activity.accelerometer.AccelerometerDetector;
import main.coderindigo.com.countdown.activity.accelerometer.OnStepCountChangeListener;
import main.coderindigo.com.countdown.activity.service.AlarmService;

public class AlarmNotify extends AppCompatActivity {


    private static final String TAG = AlarmNotify.class.getSimpleName();
    private SensorManager sensorManager;
    private TextView count;
    Boolean running = true;
    private int mStepCount = 0;
    Button fab;
    private AccelerometerDetector mAccelDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_notify);

        final Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
//        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
//
//        Ringtone ringtone = RingtoneManager.getRingtone(this, uri);
//        ringtone.play();
        count = (TextView) findViewById(R.id.count);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

//        Sensor countSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
//        if(countSensor != null ){
//            sensorManager.registerListener( this, countSensor, SensorManager.SENSOR_DELAY_UI);
//
//        }

        mAccelDetector = new AccelerometerDetector(sensorManager);
        mAccelDetector.setStepCountChangeListener(new OnStepCountChangeListener() {
            @Override
            public void onStepCountChange(long eventMsecTime) {
                ++mStepCount;
                count.setText(String.valueOf(mStepCount));
                if (mStepCount == 10) {
                    fab.setVisibility(View.VISIBLE);
                }
            }
        });


        fab = (Button) findViewById(R.id.dismiss_button);
        fab.setVisibility(View.INVISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                stopService(new Intent(getApplicationContext(), AlarmService.class));
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume register Listeners");
        mAccelDetector.startDetector();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause Unregister Listeners");
        mAccelDetector.stopDetector();
        mStepCount = 0;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
