package main.coderindigo.com.countdown.activity.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import main.coderindigo.com.countdown.activity.UpWithSmile;
import main.coderindigo.com.countdown.activity.model.Alarm;

/**
 * Created by sujana on 12/19/2015.
 */
public class DbHandler extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 3;

    // Database Name
    private static final String DATABASE_NAME = "alarmsManager";

    // Contacts table name
    private static final String TABLE_ALARMS = "alarms";

    // Contacts Table Columns names
    private static final String ID = "id";
    private static final String TIME = "time";
    private static final String STATUS = "status";
    private static final String TAG = DbHandler.class.getSimpleName() ;

    public DbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ALARMS_TABLE = "CREATE TABLE " + TABLE_ALARMS + "("
                + ID + " INTEGER PRIMARY KEY ,"+ TIME + " VARCHAR ,"
                + STATUS + " INTEGER" + ")";
        db.execSQL(CREATE_ALARMS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ALARMS);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    public void addContact(Alarm alarm) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(ID,alarm.getId());//id
        values.put(TIME, alarm.getAlarmTime()); // time
        values.put(STATUS, alarm.getStatus()); // status

        // Inserting Row
        db.insert(TABLE_ALARMS, null, values);
        Log.d(TAG, "-----------------------------------------------------------------------------------------");
        db.close(); // Closing database connection
    }

    // Getting single contact
//    Alarm getContact(int id) {
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor cursor = db.query(TABLE_ALARMS, new String[] { KEY_ID,
//                        TIME, STATUS }, KEY_ID + "=?",
//                new String[] { String.valueOf(id) }, null, null, null, null);
//        if (cursor != null)
//            cursor.moveToFirst();
//
//        Alarm alarm = new Alarm(Integer.parseInt(cursor.getString(0)),
//                cursor.getString(1), cursor.getInt(2));
//        // return contact
//        return alarm;
//    }

    // Getting All alarms
    public List<Alarm> getAllContacts() {
        List<Alarm> alarmsList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ALARMS;

        SQLiteDatabase db =this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Alarm alarm = new Alarm();
                alarm.setId(cursor.getInt(0));
                alarm.setAlarmTime(cursor.getString(1));
                alarm.setStatus(cursor.getInt(2));
                // Adding contact to listku7rv.setAdapter(recyclerViewAdapter);
                alarmsList.add(alarm);
            } while (cursor.moveToNext());
        }
cursor.close();
        // return contact list
        return alarmsList;
    }

     //Updating single contact
    public int updateContact(Alarm alarm) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        //values.put(ID,alarm.getId());
        values.put(TIME, alarm.getAlarmTime());
        values.put(STATUS, alarm.getStatus());
        Log.d(TAG, "-----" +alarm.getId() +"  ===" + alarm.getStatus() + "") ;
        // updating row
        return db.update(TABLE_ALARMS, values, ID + " = ?",
                new String[] { String.valueOf(alarm.getId()) });


    }

    // Deleting single contact
    public void deleteContact(int id) {
        Log.d(TAG, "--------------------"+ id);
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ALARMS, ID + " = ?",
                new String[] { String.valueOf(id)});
        Log.d(TAG, "-----" + id);
        db.close();
    }


    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_ALARMS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }


}
