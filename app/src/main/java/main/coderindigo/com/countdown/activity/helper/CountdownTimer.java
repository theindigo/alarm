package main.coderindigo.com.countdown.activity.helper;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;

/**
 * Created by sujana on 12/14/2015.
 */
public abstract class CountdownTimer  implements Runnable {

    private static final String TAG = CountdownTimer.class.getSimpleName();
    private long timeRemaining;
    private Handler handler;
    private boolean isKilled = false;

    public CountdownTimer(Handler handler) {
        this.handler = handler;
    }

    public CountdownTimer(Handler handler, long timeRemaining) {
        this.handler = handler;
        this.timeRemaining = timeRemaining;

    }

    //set time to run the timer
    public void setTimeRemaining(long timeRemaining) {
        this.timeRemaining = timeRemaining;
    }

    //helps start the timer
    public void start() {
        isKilled = false;
        handler.postDelayed(this, 1000);
    }

    //stops the timer
    public void stop() {
        isKilled = true;
        onTimerStopped();
    }


    @Override
    public void run() {
        //if timer is on
        if (!isKilled) {
            // callback to main to update UI with new time
            updateUI(timeRemaining);
            timeRemaining = timeRemaining - 1000;
            //when 30 sec remaining play the sound alert
            if (timeRemaining == 30000){
                Log.d(TAG, timeRemaining +"");
                onPlayNotification();
            }

            if (timeRemaining >= 0) {
                Log.d(TAG, "timer nunn");
                //call run evry second if timeremaining is not 0
                handler.postDelayed(this, 1000);
            } else {
                onTimerFinished();
            }
        }

    }

    public abstract void onPlayNotification();

    public abstract void onTimerFinished();

    public abstract void onTimerStopped();

    //check if the timer value is valid format " 00:00 "
    public static boolean isValid(String input) {
        if ((input == null) || input.isEmpty()) {
            return false;
        }
        if (input.length() == 5 && input.indexOf(":") == 2) {
            try {
                long totalDuration = covertTomili(input);
                boolean valid = (totalDuration > 30);
                return valid;
            } catch (NumberFormatException e) {
                return false;
            }
        } else {
            return false;
        }
    }
    //get the minutes in long taking a string
    public static long getMin(String input) throws NumberFormatException {
        int min = Integer.parseInt(input.substring(0, 2));
        return min;

    }
    //get the seconds in long taking a string
    public static long getSec(String input) throws NumberFormatException {
        int seconds = Integer.parseInt(input.substring(3, input.length()));
        return seconds;

    }
    // convert timee to miliseconds , this is for run method
    public static long covertTomili(String input) {
        try {
            long millisec =  ((getMin(input) * 60 + getSec(input)) * 1000);
            return millisec;
        } catch (NumberFormatException e) {
            //invalid number
            return 0;
        }
    }

    //convert timeremaining to string to display
    public static String covertToString(long input) {
        int totalsec = (int) input / 1000;
        int min = totalsec / 60;
        int sec = totalsec % 60;
        String minutesString = (min < 10) ? "0" + min : min + "";
        String secString = (sec < 10) ? "0" + sec : sec + "";
        return minutesString + ":" + secString;

    }

    //abstract method
    public abstract void updateUI(long time);
}
