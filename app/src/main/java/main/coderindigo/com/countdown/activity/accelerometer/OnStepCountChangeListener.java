package main.coderindigo.com.countdown.activity.accelerometer;

/**
 * Created by sujana on 12/26/2015.
 */
public interface OnStepCountChangeListener {
    void onStepCountChange(long eventMsecTime);
}