package main.coderindigo.com.countdown.activity.helper;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.Collections;

import main.coderindigo.com.countdown.R;

/**
 * Created by sujana on 12/21/2015.
 */
public class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener {
    private static final String TAG = RecyclerViewTouchListener.class.getSimpleName();
    private GestureDetector gestureDetector;
    ClickListener clickListener;
    
    private RecyclerView recyclerView;
    private int mSlop;
    private int mMinFlingVelocity;
    private int mMaxFlingVelocity;
    private long mAnimationTime;
    private boolean mPaused;

    final int SWIPE_THRESHOLD = 100;
    final public int SWIPE_VELOCITY_THRESHOLD = 100;

    //constructor
    public RecyclerViewTouchListener(Context context, final RecyclerView rv, final ClickListener clickListener) {
        Log.d(TAG, "RecyclerViewTouchListener: ++++");
        this.recyclerView = rv;
        this.clickListener = clickListener;
//        ViewConfiguration vc = ViewConfiguration.get(recyclerView.getContext());
//        mSlop = vc.getScaledTouchSlop();
//        mMinFlingVelocity = vc.getScaledMinimumFlingVelocity() * 16;
//        mMaxFlingVelocity = vc.getScaledMaximumFlingVelocity();
//        mAnimationTime = recyclerView.getContext().getResources().getInteger(
//                android.R.integer.config_shortAnimTime);
//
//
//
//        /**
//         * This will ensure that this SwipeableRecyclerViewTouchListener is paused during list view scrolling.
//         * If a scroll listener is already assigned, the caller should still pass scroll changes through
//         * to this listener.
//         */
//        rv.setOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                setEnabled(newState != RecyclerView.SCROLL_STATE_DRAGGING);
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//            }
//        });
//


        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                //super.onSingleTapUp(e);
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                // super.onLongPress(e);
                View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if (childView != null && clickListener != null) {

                    clickListener.onLongClick(childView.findViewById(R.id.card_touch), recyclerView.getChildLayoutPosition(childView));
                }
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                View childView = recyclerView.findChildViewUnder(e1.getX(), e1.getY());
                Log.d(TAG, "onFling" + childView);
                float distanceX = e2.getX() - e1.getX();
                float distanceY = e2.getY() - e1.getY();
                if (Math.abs(distanceX) > Math.abs(distanceY) && Math.abs(distanceX) > 100 && Math.abs(velocityX) > 100) {
                    if (distanceX > 0) {
                        Log.d(TAG, "onFlingRight xxxxxxxxx" + distanceX);
                        clickListener.onSwipeRight(recyclerView, recyclerView.getChildLayoutPosition(childView));
                    } else {
                        Log.d(TAG, "onFlingLeft yyyy" + distanceY);
                        clickListener.onSwipeLeft(recyclerView, recyclerView.getChildLayoutPosition(childView));
                    }

                    return true;
                }
                return false;
            }

        });

    }

    public void setEnabled(boolean enabled) {
        mPaused = !enabled;
    }



    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

        View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());


        if (childView != null && clickListener != null && gestureDetector.onTouchEvent(e)) {

            clickListener.onClick(childView.findViewById(R.id.card_touch), rv.getChildLayoutPosition(childView));
            Log.d(TAG, "RecyclerViewTouchListener: +11111111111111111+++" + childView.getId() + " ++++0 " + childView.findViewById(R.id.card_touch).getId());
        }


        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        Log.d(TAG, "RecyclerViewTouchListener: ++22222222222222222++");

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        Log.d(TAG, "RecyclerViewTouchListener: ++333333333333++");


    }


    public static interface ClickListener {
        public void onClick(View view, int position);

        public void onLongClick(View view, int position);

        public void onSwipeRight(RecyclerView rv, int position);

        public void onSwipeLeft(RecyclerView rv, int position);
    }

//    private void performDismiss(final View dismissView, final int dismissPosition) {
//        // Animate the dismissed list item to zero-height and fire the dismiss callback when
//        // all dismissed list item animations have completed. This triggers layout on each animation
//        // frame; in the future we may want to do something smarter and more performant.
//
//        final ViewGroup.LayoutParams lp = dismissView.getLayoutParams();
//        final int originalLayoutParamsHeight = lp.height;
//        final int originalHeight = dismissView.getHeight();
//
//        ValueAnimator animator = ValueAnimator.ofInt(originalHeight, 1).setDuration(mAnimationTime);
//
//        animator.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                --mDismissAnimationRefCount;
//                if (mDismissAnimationRefCount == 0) {
//                    // No active animations, process all pending dismisses.
//                    // Sort by descending position
//                    Collections.sort(mPendingDismisses);
//
//                    int[] dismissPositions = new int[mPendingDismisses.size()];
//                    for (int i = mPendingDismisses.size() - 1; i >= 0; i--) {
//                        dismissPositions[i] = mPendingDismisses.get(i).position;
//                    }
//
//                    if (mFinalDelta > 0) {
//                        mSwipeListener.onDismissedBySwipeRight(mRecyclerView, dismissPositions);
//                    } else {
//                        mSwipeListener.onDismissedBySwipeLeft(mRecyclerView, dismissPositions);
//                    }
//
//                    // Reset mDownPosition to avoid MotionEvent.ACTION_UP trying to start a dismiss
//                    // animation with a stale position
//                    mDownPosition = ListView.INVALID_POSITION;
//
//                    ViewGroup.LayoutParams lp;
//                    for (PendingDismissData pendingDismiss : mPendingDismisses) {
//                        // Reset view presentation
//                        pendingDismiss.view.setAlpha(mAlpha);
//                        pendingDismiss.view.setTranslationX(0);
//
//                        lp = pendingDismiss.view.getLayoutParams();
//                        lp.height = originalLayoutParamsHeight;
//
//                        pendingDismiss.view.setLayoutParams(lp);
//                    }
//
//                    // Send a cancel event
//                    long time = SystemClock.uptimeMillis();
//                    MotionEvent cancelEvent = MotionEvent.obtain(time, time,
//                            MotionEvent.ACTION_CANCEL, 0, 0, 0);
//                    mRecyclerView.dispatchTouchEvent(cancelEvent);
//
//                    mPendingDismisses.clear();
//                    mAnimatingPosition = ListView.INVALID_POSITION;
//                }
//            }
//        });
//
//        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator valueAnimator) {
//                lp.height = (Integer) valueAnimator.getAnimatedValue();
//                dismissView.setLayoutParams(lp);
//            }
//        });
//
//        mPendingDismisses.add(new PendingDismissData(dismissPosition, dismissView));
//        animator.start();
//    }


}
